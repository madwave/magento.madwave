<?php
/**
 * magento
 *
 * @author Alexander Demchenko <strong.barnaul@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Madwave\API\Service;


use Madwave\API\Service\Import\AttributeService;
use Magento\Catalog\Api\CategoryLinkManagementInterface;
use Magento\Catalog\Api\Data\ProductExtensionInterface;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Model\Stock\Item;
use Magento\Framework\ObjectManagerInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Eav\Api\Data\AttributeOptionInterface;
use Magento\Eav\Model\Config;
use Magento\Catalog\Model\Category;


/**
 * Class ProductService
 * @package Madwave\API\Service
 */
class ProductService
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;
    /**
     * @var CategorySetup
     */
    private $categorySetup;
    /**
     * @var Config
     */
    private $eavConfig;
    /**
     * @var ClientService
     */
    private $clientService;

    /**
     * @var mixed
     */
    private $attributeSetId;

    /**
     * @var array
     */
    protected $attributes = [
        AttributeService::ATTR_TYPE,
        AttributeService::ATTR_COLOR,
        AttributeService::ATTR_GENDER,
        AttributeService::ATTR_SIZE,
    ];
    /**
     * @var ProductAttributeRepositoryInterface
     */
    private $attributeRepository;

    /**
     * ProductService constructor.
     * @param ProductRepositoryInterface $productRepository
     * @param ObjectManagerInterface $objectManager
     * @param CategorySetup $categorySetup
     * @param Config $eavConfig
     * @param ClientService $clientService
     * @param ProductAttributeRepositoryInterface $attributeRepository
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        ObjectManagerInterface $objectManager,
        CategorySetup $categorySetup,
        Config $eavConfig,
        ClientService $clientService,
        ProductAttributeRepositoryInterface $attributeRepository
    )
    {
        $this->productRepository = $productRepository;
        $this->objectManager = $objectManager;
        $this->categorySetup = $categorySetup;
        $this->eavConfig = $eavConfig;
        $this->clientService = $clientService;
        $this->attributeSetId = $this->categorySetup->getAttributeSetId(Product::ENTITY, 'Default');
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Exception
     */
    public function createProduct()
    {
        /** @var Factory $optionsFactory */
        $optionsFactory = $this->objectManager->create(Factory::class);

        foreach ($this->getProducts()->data as $apiProductData) {
            try {
                $size = false;
                $color = false;

                $productDetailData = $this->getProduct($apiProductData->id);
                $associatedProductIds = $this->getAssociatedProductIds($productDetailData->data, $size, $color);

                $configurableAttributesData = $this->getConfigurableAttributesData($productDetailData->data, $size, $color);
                $configurableOptions = $optionsFactory->create($configurableAttributesData);

                /** @var $product Product */
                $product = $this->objectManager->create(Product::class);

                /** @var ProductExtensionInterface $extensionConfigurableAttributes */
                $extensionConfigurableAttributes = $product->getExtensionAttributes();
                $extensionConfigurableAttributes->setConfigurableProductOptions($configurableOptions);
                $extensionConfigurableAttributes->setConfigurableProductLinks($associatedProductIds);

                $product->setTypeId(Configurable::TYPE_CODE)
                    ->setId($apiProductData->id)
                    ->setExtensionAttributes($extensionConfigurableAttributes)
                    ->setAttributeSetId($this->attributeSetId)
                    ->setWebsiteIds([1])
                    ->setName($apiProductData->name)
                    ->setSku(uniqid())
                    ->setVisibility(Visibility::VISIBILITY_BOTH)
                    ->setStatus(Status::STATUS_ENABLED)
                    ->setStockData(
                        [
                            'use_config_manage_stock' => 0,
                            'min_sale_qty' => 1,
                            'is_in_stock' => 1,
//                            'qty' => $productVariantData->stock,
                        ]
                    );

                $this->productRepository->save($product);
//            $this->assignStockItem($product->getId(), $qty);

                /** @var Category $category */
                $category = $this->objectManager
                    ->create(Category::class)
                    ->loadByAttribute('erp_id', $apiProductData->category_id);

                if ($category !== false) {
                    /** @var CategoryLinkManagementInterface $categoryLinkManagement */
                    $categoryLinkManagement = $this->objectManager->create(CategoryLinkManagementInterface::class);
                    $categoryLinkManagement->assignProductToCategories(
                        $product->getSku(),
                        [$category->getId()]
                    );
                }
            } catch (\Exception $exception) {
                echo $exception->getMessage() . "\n";
            }
        }
    }

    /**
     * @param $productVariantData
     * @param $productData
     * @return \Magento\Catalog\Api\Data\ProductInterface|Product
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Exception
     */
    protected function createSimpleProduct($productVariantData, $productData)
    {
        /** @var $product Product */
        $product = $this->objectManager->create(Product::class);
        $product->setTypeId(Type::TYPE_SIMPLE)
            ->setId($productVariantData->id)
            ->setAttributeSetId($this->attributeSetId)
            ->setWebsiteIds([1])
            ->setUrlKey(str_replace(' ', '-', $productData->name) . '-' . microtime())
            ->setName($productData->name)
            ->setSku($productVariantData->article)
            ->setPrice($productVariantData->price_rrp)
            ->setVisibility(Visibility::VISIBILITY_NOT_VISIBLE)
            ->setStatus(Status::STATUS_ENABLED)
            ->setCustomAttribute('color', $this->getOptions('color')[$productVariantData->color] ?? null)
            ->setCustomAttribute('type', $this->getOptions('type')[$productData->type] ?? null)
            ->setCustomAttribute('gender', $this->getOptions('gender')[$productData->gender] ?? null)
            ->setCustomAttribute('size', $this->getOptions('size')[$productVariantData->size] ?? null)
            ->setStockData(
                [
                    'use_config_manage_stock' => 0,
                    'min_sale_qty' => 1,
                    'is_in_stock' => 1,
                    'qty' => $productVariantData->stock,
                ]
            );
        $product = $this->productRepository->save($product);
//        $this->assignStockItem($product->getId(), $productVariantData->stock);


        return $product;
    }

    /**
     * @param int $page
     * @return mixed
     */
    public function getProducts($page = 1)
    {
        $content = $this->clientService->getClient()
            ->get('products', [
                'query' => ['page' => $page],
            ])
            ->getBody()
            ->getContents();
        return json_decode($content);
    }

    /**
     * @param $product
     * @return mixed
     */
    protected function getProductVariants($product)
    {
        $content = $this->clientService->getClient()
            ->get('products/' . $product->id . '/variants')
            ->getBody()
            ->getContents();
        return json_decode($content);
    }

    /**
     * @param $product
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getConfigurableAttributesData($product, $size, $color)
    {
        $configurableAttributesData = [];
        foreach ($this->attributes as $attrCode) {

            if ($attrCode == 'type' && !isset($this->getOptions('type')[$product->type])) {
                continue;
            }

            if ($attrCode == 'gender' && !isset($this->getOptions('gender')[$product->gender])) {
                continue;
            }

            if ($attrCode == 'size' && !$size) {
                continue;
            }

            if ($attrCode == 'color' && !$color) {
                continue;
            }

            $attribute = $this->eavConfig->getAttribute(Product::ENTITY, $attrCode);
            /** @var AttributeOptionInterface[] $options */
            $options = $attribute->getOptions();

            $attributeValues = [];
            array_shift($options); //remove the first option which is empty

            foreach ($options as $option) {
                $attributeValues[] = [
                    'label' => $option->getLabel(),
                    'attribute_id' => $attribute->getId(),
                    'value_index' => $option->getValue(),
                ];
            }

            $configurableAttributesData[] = [
                'attribute_id' => $attribute->getId(),
                'code' => $attribute->getAttributeCode(),
                'label' => $attribute->getStoreLabel(),
                'position' => '0',
                'values' => $attributeValues,
            ];

        }

        return $configurableAttributesData;
    }

    /**
     * @param $product
     * @param $size
     * @param $color
     * @return array
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     */
    protected function getAssociatedProductIds($product, &$size, &$color)
    {
        $size = true;
        $color = true;
        $associatedProductIds = [];
        foreach ($this->getProductVariants($product)->data as $productVariant) {
            if (!isset($this->getOptions('color')[$productVariant->color])) {
                $color = false;
            }
            if (!isset($this->getOptions('size')[$productVariant->size])) {
                $size = false;
            }
            $simple = $this->createSimpleProduct($productVariant, $product);
            $associatedProductIds[] = $simple->getId();
        }
        return $associatedProductIds;
    }

    /**
     * @param $productId
     * @param $qty
     */
    protected function assignStockItem($productId, $qty)
    {
        /** @var Item $stockItem */
        $stockItem = $this->objectManager
            ->create(Item::class);
        $stockItem->setProductId($productId);
        $stockItem->setUseConfigManageStock(1);
        $stockItem->setQty($qty);
        $stockItem->setIsQtyDecimal(0);
        $stockItem->setIsInStock(1);
        try {
            $stockItem->save();
        } catch (\Exception $e) {

        }
    }

    /**
     * @param $code
     * @return array
     */
    private function getOptions($code)
    {
        try {
            $attribute = $this->attributeRepository->get($code);
            $options = [];
            foreach ($attribute->getOptions() as $option) {
                $options[$option->getLabel()] = $option->getValue();
            }
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $options = [];
        }
        return $options;
    }

    /**
     * @param $id
     * @return mixed
     */
    private function getProduct($id)
    {
        $content = $this->clientService->getClient()
            ->get('products/' . $id)
            ->getBody()
            ->getContents();
        return json_decode($content);
    }

}
