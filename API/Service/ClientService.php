<?php
/**
 * magento
 *
 * @author Alexander Demchenko <strong.barnaul@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Madwave\API\Service;


use GuzzleHttp\Client;
use Magento\Framework\App\DeploymentConfig;

/**
 * Class ClientService
 * @package Madwave\API\Service
 */
class ClientService
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var DeploymentConfig
     */
    private $deploymentConfig;

    /**
     * ClientService constructor.
     * @param DeploymentConfig $deploymentConfig
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\RuntimeException
     */
    public function __construct(DeploymentConfig $deploymentConfig)
    {
        $this->deploymentConfig = $deploymentConfig;
        $this->client = new Client([
            'base_uri' => $this->deploymentConfig->get('madwave_api/url'),
            'headers' => [
                'Authorization' => "Bearer " . $this->deploymentConfig->get('madwave_api/token'),
            ]
        ]);
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }
}