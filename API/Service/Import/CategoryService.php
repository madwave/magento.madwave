<?php
/**
 * magento
 *
 * @author Alexander Demchenko <strong.barnaul@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Madwave\API\Service\Import;


use Madwave\API\Service\ClientService;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\Category;

/**
 * Class CategoryService
 * @package Madwave\API\Service\Import
 */
class CategoryService
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ClientService
     */
    private $clientService;

    /**
     * @var EavSetup
     */
    private $eavSetup;

    /**
     * @var AttributeRepositoryInterface
     */
    private $attributeRepository;

    /**
     * CategoryService constructor.
     * @param ObjectManagerInterface $objectManager
     * @param CategoryRepositoryInterface $categoryRepository
     * @param StoreManagerInterface $storeManager
     * @param ClientService $clientService
     * @param EavSetup $eavSetup
     * @param AttributeRepositoryInterface $attributeRepository
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        CategoryRepositoryInterface $categoryRepository,
        StoreManagerInterface $storeManager,
        ClientService $clientService,
        EavSetup $eavSetup,
        AttributeRepositoryInterface $attributeRepository
    )
    {
        $this->objectManager = $objectManager;
        $this->categoryRepository = $categoryRepository;
        $this->storeManager = $storeManager;
        $this->clientService = $clientService;
        $this->eavSetup = $eavSetup;
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function createEAVExternalId()
    {
        $this->eavSetup->addAttribute(
            Category::ENTITY,
            'erp_id',
            [
                'type' => 'int',
                'label' => 'ERP ID',
                'input' => 'text',
                'frontend' => '',
                'required' => false,
                'backend' => '',
                'sort_order' => '30',
                'global' => 1,
                'default' => null,
                'visible' => true,
                'user_defined' => false,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'unique' => true,
                'group' => 'General',
                'used_in_product_listing' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'option' => ''
            ]);
    }

    public function import()
    {
	$this->createEAVExternalId();

        $categories = $this->clientService->getClient()
            ->get('categories')
            ->getBody()
            ->getContents();

        $categories = json_decode($categories);

        $categories2 = $this->clientService->getClient()
            ->get('categories?page=2')
            ->getBody()
            ->getContents();

        $categories2 = json_decode($categories2);

        $result = array_merge($categories->data, $categories2->data);

        $map = [];

        foreach ($result as $category) {
            $map[$category->parent_id][$category->id] = $category;
        }

        $create = function ($category, $parentId) use (&$create, $map) {
            $cat = $this->createCategory($category, $parentId);
            if (isset($map[$category->id])) {
                foreach ($map[$category->id] as $item) {
                    $create($item, $cat->getId());
                }
            }
        };

        foreach ($categories->data as $category) {
            if ($category->parent_id) {
                continue;
            }
            $create($category, null);
        }
    }

    /**
     * @param $category
     * @param $parentId
     * @return Category
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    protected function createCategory($category, $parentId = null)
    {
        $category = $this->objectManager
            ->create(Category::class, [
                'data' =>
                    [
                        'erp_id' => $category->id,
                        "name" => $category->name,
                        "parent_id" => $parentId,
                        "position" => 10,
                        "is_active" => true,
                        "include_in_menu" => true,
                    ],
                'custom_attributes' =>
                    [
                        "display_mode" => "PRODUCTS",
                        "is_anchor" => "1",
                    ]
            ]);
        $this->categoryRepository->save($category);
        return $category;
    }
}
