<?php
/**
 * magento
 *
 * @author Alexander Demchenko <strong.barnaul@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Madwave\API\Service\Import;

use Laminas\Form\Element\Color;
use Madwave\API\Service\ClientService;
use Magento\Eav\Api\AttributeSetRepositoryInterface;
use Magento\Eav\Model\AttributeManagement;
use Magento\Eav\Model\AttributeSetManagement;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Eav\Model\Entity\TypeFactory;
use Magento\Eav\Setup\EavSetup;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
//use Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory;
use Magento\Catalog\Setup\CategorySetup;
use Magento\Framework\App\Bootstrap;


/**
 * Class AttributeService
 * @package Madwave\API\Service\Import
 */
class AttributeService
{
    const ATTR_COLOR = 'color';
    const ATTR_GENDER = 'gender';
    const ATTR_SIZE = 'size';
    const ATTR_TYPE = 'type';

    /**
     * @var string
     */
    protected $entityTypeCode = 'catalog_product';

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * @var AttributeSetRepositoryInterface
     */
    private $attributeSetRepository;

    /**
     * @var TypeFactory
     */
    private $typeFactory;

    /**
     * @var AttributeSetManagement
     */
    private $attributeSetManagement;

    /**
     * @var AttributeManagement
     */
    private $attributeManagement;

    /**
     * @var EavSetup
     */
    private $eavSetup;
    /**
     * @var ClientService
     */
    private $clientService;

    /**
     * AttributeService constructor.
     * @param AttributeSetFactory $attributeSetFactory
     * @param AttributeSetRepositoryInterface $attributeSetRepository
     * @param TypeFactory $typeFactory
     * @param AttributeSetManagement $attributeSetManagement
     * @param AttributeManagement $attributeManagement
     * @param EavSetup $eavSetup
     * @param ClientService $clientService
     */
    public function __construct(
        AttributeSetFactory $attributeSetFactory,
        AttributeSetRepositoryInterface $attributeSetRepository,
        TypeFactory $typeFactory,
        AttributeSetManagement $attributeSetManagement,
        AttributeManagement $attributeManagement,
        EavSetup $eavSetup,
        ClientService $clientService
    )
    {
        $this->attributeSetFactory = $attributeSetFactory;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->typeFactory = $typeFactory;
        $this->attributeSetManagement = $attributeSetManagement;
        $this->attributeManagement = $attributeManagement;
        $this->eavSetup = $eavSetup;
        $this->clientService = $clientService;
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function install()
    {
        $this->handleAttributes();
    }

    /**
     * @param string $name
     * @param array $result
     * @return mixed
     */
    protected function getProductProperties(string $name)
    {
        return json_decode($this->clientService
            ->getClient()
            ->get('products/properties/' . $name)
            ->getBody()
            ->getContents());
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Validate_Exception
     */
    protected function handleAttributes()
    {
        $genders = $this->getProductProperties('genders');
        $colors = $this->getProductProperties('colors');
//        $measures = $this->getProductProperties('measures');
        $sizes = $this->getProductProperties('sizes');
        $types = $this->getProductProperties('types');


        $this->create(self::ATTR_GENDER, 'Gender', 'int', 'select', $genders->data);
        $this->create(self::ATTR_COLOR, 'Color', 'int', 'select', $colors->data);
        $this->create(self::ATTR_SIZE, 'Size', 'int', 'select', $sizes->data);
        $this->create(self::ATTR_TYPE, 'Type', 'int', 'select', $types->data);

    }

    /**
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function attributeSet()
    {
        $entityType = $this->typeFactory->create()->loadByCode($this->entityTypeCode);
        $defaultSetId = $entityType->getDefaultAttributeSetId();
        $dataAttr = [
            [
                'attribute_set_name' => 'Color',
                'entity_type_id' => $entityType->getId(),
                'sort_order' => 200,
            ],
            [
                'attribute_set_name' => 'SIZES',
                'entity_type_id' => $entityType->getId(),
                'sort_order' => 300,
            ]
        ];

        foreach ($dataAttr as $attr) {
            $attributeSet = $this->attributeSetFactory->create();

            $attributeSet->setData($attr);

            $this->attributeSetManagement->create($this->entityTypeCode, $attributeSet, $defaultSetId);

            $this->attributeManagement->assign(
                $this->entityTypeCode,
                $attributeSet->getId(),
                $attributeSet->getDefaultGroupId(),
                'attribute_code',
                100
            );
        }
    }

    /**
     * @param $attribute
     * @param $label
     * @param string $type
     * @param string $input
     * @param array $data
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function create($attribute, $label, $type = 'text', $input = 'text', $data = [])
    {
        $this->eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, $attribute);
        $this->eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            $attribute,
            [
                'type' => $type,
                'label' => $label,
                'input' => $input,
                'frontend' => '',
                'required' => false,
                'backend' => '',
                'sort_order' => '30',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'default' => null,
                'visible' => true,
                'user_defined' => true,
                'searchable' => true,
                'filterable' => true,
                'comparable' => true,
                'visible_on_front' => true,
                'unique' => false,
                'apply_to' => 'simple,grouped,bundle,configurable,virtual',
                'group' => 'General',
                'used_in_product_listing' => true,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'option' => ''
            ]
        );

        $attributeId = $this->eavSetup
            ->getAttributeId(\Magento\Catalog\Model\Product::ENTITY, $attribute);

        $values = [];
        foreach ($data as $datum) {
            $values[$datum->id] = $datum->name;
        }
        if ($attribute == self::ATTR_SIZE) {
            $values[] = 'One size';
        }
        $options = [
            'values' => $values,
            'attribute_id' => $attributeId,
        ];
        $this->eavSetup->addAttributeOption($options);
    }

    /**
     * @return array
     */
    public function getAttributeIds()
    {
        return [
            $this->eavSetup
                ->getAttributeId(\Magento\Catalog\Model\Product::ENTITY, self::ATTR_TYPE),
            $this->eavSetup
                ->getAttributeId(\Magento\Catalog\Model\Product::ENTITY, self::ATTR_SIZE),
            $this->eavSetup
                ->getAttributeId(\Magento\Catalog\Model\Product::ENTITY, self::ATTR_COLOR),
            $this->eavSetup
                ->getAttributeId(\Magento\Catalog\Model\Product::ENTITY, self::ATTR_GENDER),
        ];
    }
}
