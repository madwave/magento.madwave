<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Madwave_API
 * @author     Webkul
 * @copyright  Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */

namespace Madwave\API\Cron;

use Madwave\API\Service\Import\AttributeService;
use Madwave\API\Service\Import\CategoryService;
use Madwave\API\Service\Import\ProductService;

/**
 * Cron class.
 */
class ImportFromErp
{
    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var CategoryService
     */
    private $categoryService;

    /**
     * @var AttributeService
     */
    private $attributeService;

    /**
     * ImportFromErp constructor.
     * @param ProductService $productService
     * @param CategoryService $categoryService
     * @param AttributeService $attributeService
     */
    public function __construct(
        ProductService $productService,
        CategoryService $categoryService,
        AttributeService $attributeService
    )
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
        $this->attributeService = $attributeService;
    }

    /**
     *
     */
    public function execute()
    {
        echo '00000000000';
//        $this->productService->handleProducts();
        echo 1111111;
    }
}
