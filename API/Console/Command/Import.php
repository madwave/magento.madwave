<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Madwave_API
 * @author     Webkul
 * @copyright  Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */

namespace Madwave\API\Console\Command;

use Madwave\API\Service\Import\AttributeService;
use Madwave\API\Service\Import\CategoryService;
//use Madwave\API\Service\Import\ProductService;
use Madwave\API\Service\ProductService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command class.
 */
class Import extends Command
{
    /**
     * @var AttributeService
     */
    private $attributeService;

    /**
     * @var CategoryService
     */
    private $categoryService;
    /**
     * @var \Madwave\API\Service\ProductService
     */
    private $product;

    private $state;

    /**
     * ImportFromErp constructor.
     * @param AttributeService $attributeService
     * @param CategoryService $categoryService
     * @param ProductService $product
     */
    public function __construct(
        AttributeService $attributeService,
        CategoryService $categoryService,
        ProductService $product,
	\Magento\Framework\App\State $state
    )
    {
        $this->attributeService = $attributeService;
        $this->categoryService = $categoryService;
        $this->product = $product;
	$this->state = $state;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('madwave:api:import');
        $this->setDescription('madwave:api:import');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Validate_Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
	  $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
//	  $this->categoryService->import();
//        $this->attributeService->install();
        $this->product->createProduct();
        $output->writeln("done.");
    }
}
